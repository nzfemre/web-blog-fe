export const postTemplate = (
  title,
  context,
  date,
  userId,
  id,
  isUserLiked,
  isUserDisliked
) => {
  return `
                      <div class="card mb-4">
                          <img src="img/game.png" class=" card-img-top img-thumbnail" alt="">
                          <div class="card-body">
                              <h4 class="card-title">${title}</h4>
                              <p class="card-text">${context}</p>
                              <ul class="list-unstyled like-buttons">
                                  ${
                                    userId == 1
                                      ? `<li><button onclick="deletePost(${id})" type="button" class="btn btn-warning"><i
                                              class=""></i>&nbsp;Sil</button> </li>`
                                      : ""
                                  }
                                  
                                  ${
                                    userId
                                      ? `${
                                          isUserLiked
                                            ? "Beğenildi"
                                            : `<li><button onclick="likePost(${id})" type="button" class="btn btn-info"><i
                                              class="far fa-heart"></i>&nbsp;Like</button> </li>`
                                        }
  
                                      ${
                                        isUserDisliked
                                          ? "Dislike Atıldı"
                                          : `<li><button onclick="unlikePost(${id})" type="button" class="btn btn-danger"><i
                                              class="far fa-thumbs-down"></i></i>&nbsp;Dislike</button>`
                                      }`
                                      : ""
                                  }
                                  </li>
                                  <li class="post-categori">
                                      <div class="dropdown mt-3 d-inline" style="float: right;">
                                          ${
                                            userId
                                              ? `<button class="btn-sm btn-info dropdown-toggle" data-toggle="dropdown">
                                                  Yorum Seçenekleri
                                              </button>
                                              <div class="dropdown-menu">
                                                  <a href="" class="dropdown-item">Yorum Ekle</a>
                                                  <a href="" class="dropdown-item">Yorum Sil</a>
                                                  <a href="" class="dropdown-item">Yorum Düzenle </a>
                                              </div>`
                                              : ""
                                          }
                                          <div class="dropdown mt-3 d-inline">
                                              <button class="btn-sm btn-info dropdown-toggle"
                                                  data-toggle="dropdown">Paylaş</button>
                                              <div class="dropdown-menu">
                                                  <a href="" class="dropdown-item">Facebook</a>
                                                  <a href="" class="dropdown-item">Whatsapp</a>
                                                  <a href="" class="dropdown-item">İnstagram</a>
                                                  <a href="" class="dropdown-item">Twitter</a>
                                              </div>
                                          </div>
                                  </li>
                              </ul>
                          </div>
                          <div class="card-footer text-muted">
                              ${date} Tarihinde Yazıldı.
                          </div>
                      </div>`;
};
