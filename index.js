const id = localStorage.getItem("id");
if (id) {
  $("#login-buton").hide();
}
if (!id || id !== "1") {
  $("#post-operations").hide();
}

$("#logout-button").click(function () {
  localStorage.removeItem("id");
  location.reload();
});

$.ajax({
  type: "GET",
  url: "https://warm-dawn-88942.herokuapp.com/param",
}).done((res) => {
  $("#phone").text(res.find((param) => param.name === "PHONE_NUMBER").value);
  $("#email").text(res.find((param) => param.name === "MAIL").value);
  $("#desc").text(res.find((param) => param.name === "PAGE_DESC").value);
});

$.ajax({
  type: "GET",
  url: "https://warm-dawn-88942.herokuapp.com/post",
}).done((res) => {
  res.forEach((post) => {
    const isUserLiked = post.likes
      ?.filter((p) => p.likeAction == "LIKE")
      .map((p) => "" + p.userId)
      .includes(id);
    const isUserDisliked = post.likes
      ?.filter((p) => p.likeAction == "DISLIKE")
      .map((p) => "" + p.userId)
      .includes(id);

    $("#posts").append(
      postTemplate(
        post.title,
        post.category.name,
        post.context,
        post.date,
        id,
        post.id,
        isUserLiked,
        isUserDisliked
      )
    );
  });
});

const deletePost = (id) => {
  $.ajax({
    type: "DELETE",
    url: "https://warm-dawn-88942.herokuapp.com/post/" + id,
    success: (res) => {
      window.location.replace("home.html");
    },
  });
};

const likePost = (id) => {
  saveLike(id, 0);
};

const unlikePost = (id) => {
  saveLike(id, 1);
};

const saveLike = (postId, action) => {
  $.ajax({
    type: "POST",
    url: "https://warm-dawn-88942.herokuapp.com/like",
    data: JSON.stringify({
      postId: postId,
      userId: id,
      likeAction: action,
    }),
    headers: {
      "Content-Type": "application/json",
    },
    success: (res) => {
      window.location.replace("home.html");
    },
  });
};

const search = () => {
  const category = $("#search").val();
  $("#posts").empty();

  $.ajax({
    type: "GET",
    url: "https://warm-dawn-88942.herokuapp.com/post",
  }).done((res) => {
    res
      .filter((post) => {
        if (!category || category === "") return true;
        return post.category.name === category;
      })
      .forEach((post) => {
        const isUserLiked = post.likes
          ?.filter((p) => p.likeAction == "LIKE")
          .map((p) => "" + p.userId)
          .includes(id);
        const isUserDisliked = post.likes
          ?.filter((p) => p.likeAction == "DISLIKE")
          .map((p) => "" + p.userId)
          .includes(id);

        $("#posts").append(
          postTemplate(
            post.title,
            post.category.name,
            post.context,
            post.date,
            id,
            post.id,
            isUserLiked,
            isUserDisliked
          )
        );
      });
  });
};

const randomPost = () => {
  $("#posts").empty();

  $.ajax({
    type: "GET",
    url: "https://warm-dawn-88942.herokuapp.com/post",
  }).done((res) => {
    const post = res[Math.floor(Math.random() * res.length)];
    const isUserLiked = post.likes
      ?.filter((p) => p.likeAction == "LIKE")
      .map((p) => "" + p.userId)
      .includes(id);
    const isUserDisliked = post.likes
      ?.filter((p) => p.likeAction == "DISLIKE")
      .map((p) => "" + p.userId)
      .includes(id);

    $("#posts").append(
      postTemplate(
        post.title,
        post.category.name,
        post.context,
        post.date,
        id,
        post.id,
        isUserLiked,
        isUserDisliked
      )
    );
  });
};

const reset = () => {
  $("#search").val("");
  search();
};

const showSuggestions = () => {
  $.ajax({
    type: "GET",
    url: "https://warm-dawn-88942.herokuapp.com/suggestion",
  }).done((res) => {
    res.forEach((suggestion) => {
      $("#suggestionList").append(suggestionTemplate(suggestion.context));
    });
  });

  $("#suggestions").modal("show");
};

const onCloseSuggestions = () => {
  $("#suggestionList").empty();
  $("#suggestions").modal("hide");
};
/**
 * Templates
 */

const postTemplate = (
  title,
  category,
  context,
  date,
  userId,
  id,
  isUserLiked,
  isUserDisliked
) => {
  return `
                      <div class="card mb-4">
                          <div class="card-body">
                          <div class="row">
                            <div class="col-sm-9">
                                <h4 class="card-title">${title}</h4>
                            </div>
                            <div class="col-sm-3" style="text-align:right">
                                <h6 class="card-title">${category}</h6>
                            </div>
                          </div>
                              <p class="card-text">${context}</p>
                              <ul class="list-unstyled like-buttons">
                                  ${
                                    userId == 1
                                      ? `<li><button onclick="deletePost(${id})" type="button" class="btn btn-warning"><i
                                              class=""></i>&nbsp;Sil</button> </li>`
                                      : ""
                                  }
                                  
                                  ${
                                    userId
                                      ? `${
                                          isUserLiked
                                            ? "Beğenildi"
                                            : `<li><button onclick="likePost(${id})" type="button" class="btn btn-info"><i
                                              class="far fa-heart"></i>&nbsp;Like</button> </li>`
                                        }
  
                                      ${
                                        isUserDisliked
                                          ? "Dislike Atıldı"
                                          : `<li><button onclick="unlikePost(${id})" type="button" class="btn btn-danger"><i
                                              class="far fa-thumbs-down"></i></i>&nbsp;Dislike</button>`
                                      }`
                                      : ""
                                  }
                                  </li>
                                  <li class="post-categori">
                                      <div class="dropdown mt-3 d-inline" style="float: right;">
                                          ${
                                            userId
                                              ? `<button class="btn-sm btn-info dropdown-toggle" data-toggle="dropdown">
                                                  Yorum Seçenekleri
                                              </button>
                                              <div class="dropdown-menu">
                                                  <a href="" class="dropdown-item">Yorum Ekle</a>
                                                  <a href="" class="dropdown-item">Yorum Sil</a>
                                                  <a href="" class="dropdown-item">Yorum Düzenle </a>
                                              </div>`
                                              : ""
                                          }
                                          <div class="dropdown mt-3 d-inline">
                                              <button class="btn-sm btn-info dropdown-toggle"
                                                  data-toggle="dropdown">Paylaş</button>
                                              <div class="dropdown-menu">
                                                  <a href="" class="dropdown-item">Facebook</a>
                                                  <a href="" class="dropdown-item">Whatsapp</a>
                                                  <a href="" class="dropdown-item">İnstagram</a>
                                                  <a href="" class="dropdown-item">Twitter</a>
                                              </div>
                                          </div>
                                  </li>
                              </ul>
                          </div>
                          <div class="card-footer text-muted">
                              ${date} Tarihinde Yazıldı.
                          </div>
                      </div>`;
};

const suggestionTemplate = (suggestion) => {
  return `
  <div class="card mb-2">
    <div class="card-body">
      <h6 class="card-subtitle mb-2 text-muted">Öneri</h6>
      <p class="card-text">${suggestion}</p>
    </div>
  </div>
`;
};
